export default {
  DAI: 'TAO',
  SAI: 'SAI',
  MKR: 'MKR',
  WETH: 'WETH',
  PETH: 'PETH',
  ETH: 'ETH'
};
